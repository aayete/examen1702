<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.*"%>
<%@ include file = "header.jsp" %>
  <main>
  
<h1>Peliculas</h1>

<%! List <Movie> movies;%>

<%
  movies = (List<Movie>)request.getAttribute("movies");
  if (movies == null) {
	  movies = new ArrayList<Movie>();
  }
%>

<table border = 1>
<tr>
	<th>Titulo</th>
	<th>Director</th>
	<th>A�o</th>
	<th>Operaciones</th>
</tr>
<% for (Movie item : movies) {%>
  <tr>
  <td><%= item.getTitle() %></td>
  <td><%= item.getDirector() %></td>
  <td><%= item.getYear() %></td>
  <td>
<%--   	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>">ver</a> --%>
<%--   	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/edit">editar</a> --%>
	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/remember">Recordar</a>
  	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/delete">borrar</a>
  </td>
  </tr>
<% } %>
</table>
  </main>
<%@ include file = "footer.jsp" %>