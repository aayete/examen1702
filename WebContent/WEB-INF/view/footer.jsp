<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.*"%>
<%! List <Movie> list;%>

<%
  list = (List<Movie>)session.getAttribute("list");
  if (list == null) {
      list = new ArrayList<Movie>();
  }
%>

  <footer>
    DAW - 2017/2018
    <ul>
    <% for (Movie item : list) {%>
	  <li><%= item.getTitle() %></li>
	<% } %>
    </ul>
    
  </footer>
</body>
</html>