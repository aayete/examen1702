<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="/Examen1702/css/default.css">
<title>Examen Java EE</title>
</head>
<body>


  <header>

    <div id="title">Examen Java EE. Curso 2017/2018</div>

    <nav>
      <ul>
        <li><a href="/Examen1702/">Inicio</a></li>
        <li><a href="/Examen1702/movies">Lista de Películas</a></li>
        <li><a href="/Examen1702/movies/create">Nueva</a></li>
      </ul>
    </nav>
  </header>
