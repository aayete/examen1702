package models;

public class Movie {

	public String title;
	public String director;
	public Integer year;
	public Integer id;
	
	
	public Movie(){
		
	}
	
	public Movie(String title, String director, Integer year) {
		super();
		this.title = title;
		this.director = director;
		this.year = year;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	
}
