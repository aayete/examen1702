package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Movie;
import persistance.MovieDAO;

/**
 * Servlet implementation class Movies
 */
@WebServlet("/movies/*")
public class MoviesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MoviesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        
        if (length == 3 || pieces[3].equals("index")) {
        	System.out.println(length);
            // "/index"
            index(request, response);
        } else if(length > 3){
        	String piece = pieces[3];
        	if (piece.equals("create")) {
              create(request, response);
          } else {
            int id = 0;
            try {
                id = Integer.parseInt(piece);
            } catch (Exception e) {
                response.sendRedirect(request.getContextPath());
            }
            if (length == 4) {
                
            } else {
                piece = pieces[4];
                if (piece.equals("remember")) {
                    remember(request, response, id);                        
                } else if (piece.equals("delete")){
                    delete(request, response, id);                        
                } else {
                    response.sendRedirect(".");
                }
            }

        }
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        if (length == 3) {
            store(request, response);
        }
	}
	
	private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/movies.jsp");
        
        ArrayList<Movie> movies = null;
        try {
            MovieDAO dao= new MovieDAO() ;
            movies = dao.all();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
        
        request.setAttribute("movies", movies);
        dispatcher.forward(request, response);
    }
	
	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/create.jsp");
        dispatcher.forward(request, response);
    }
	
	private void store(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Movie movie = new Movie(request.getParameter("titulo"), request.getParameter("director"),Integer.parseInt(request.getParameter("anio")));
        
        try{
        	MovieDAO dao = new MovieDAO();
            dao.insert(movie);
        }catch (SQLException e) {
        	System.out.println("SQLException: " + e.getMessage());
		}
        response.sendRedirect(request.getContextPath() + "/movies");
    }
	
	private void delete(HttpServletRequest request, HttpServletResponse response, int id)
            throws ServletException, IOException {
    	
    	MovieDAO borra = new MovieDAO();
    	borra.delete(id);
        response.sendRedirect(request.getContextPath() + "/movies/index");
    }
	
	private void remember(HttpServletRequest request, HttpServletResponse response, int id) throws ServletException, IOException{
		ArrayList <Movie> list;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("list") == null) {
            session.setAttribute("list", new ArrayList<Movie>());
        }
       
        list = (ArrayList<Movie>)session.getAttribute("list");
        Movie movie = new Movie();
        MovieDAO dao = new MovieDAO();
        movie = dao.get(id);
        list.add(movie);

       response.sendRedirect(request.getContextPath() + "/movies/index");
	}
	
	private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
         String[] pieces = uri.split("/");
        int i = 0;
        for (String piece : pieces) {
            i++;
        }
        return pieces;
    }

}
