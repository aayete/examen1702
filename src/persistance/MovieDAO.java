package persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import models.Movie;

public class MovieDAO {

    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/examen1702";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";
    private Connection connection = null;
    
    public MovieDAO() {
        try {
            Class.forName(DB_DRIVER).newInstance();
        } catch (Exception ex) {
            System.out.println("no se ha cargado el driver");
        }
        try {
            connection = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_USER);
            System.out.println("Conectado");

        } catch (SQLException ex) {
            System.out.println("Fallo Conexion");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (connection != null) {
            connection.close();        
        }
    }

    public ArrayList <Movie> all() throws SQLException{
        ArrayList<Movie> movies = new ArrayList<Movie>();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM movies";

        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
            	Movie movie = new Movie();
            	movie.setTitle(rs.getString("title"));
            	movie.setDirector(rs.getString("director"));
            	movie.setYear(rs.getInt("year"));
            	movie.setId(rs.getInt("id"));
            	movies.add(movie);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
        
        
        return movies;
    }
    
    public void insert(Movie movie) throws SQLException{
        PreparedStatement stmt = null;

        String sql = "INSERT INTO movies (title, director, year) VALUES (?, ?, ?);";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, movie.getTitle());
            stmt.setString(2, movie.getDirector());
            stmt.setInt(3, movie.getYear());
            stmt.executeUpdate() ;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }
    }
    
    public void delete(int id){
    	PreparedStatement stmt = null;

        String sql = "DELETE FROM movies WHERE id = " + id + "";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.executeUpdate() ;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }
    }
    
    public Movie get(int id) {
        Movie movie = new Movie();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM movies WHERE id = ?";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
            	movie.setTitle(rs.getString("title"));
            	movie.setDirector(rs.getString("director"));
            	movie.setYear(rs.getInt("year"));
            	movie.setId(rs.getInt("id"));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }             

        return movie;
        
    }

}
